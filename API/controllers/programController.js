const Program = require("./../models/Programs")

module.exports.getAllActive = () => {
	return Program.find({isActive: true}).then(result => {
		return result
	})
}


module.exports.getAllProgram = () => {
	return Program.find().then(result => {
		return result
	})
}


module.exports.addProgram = (reqBody) => {

	let newProgram = new Program({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProgram.save().then((program, err) => {
		if(err){
			return false
		} else {
			return true
		}
	});
};

module.exports.getSingleProgram = (params) => {

	return Program.findById(params.programId).then(result => {
		return result
	})
};

module.exports.editProgram = (params, reqBody) => {
	let updatedProgram = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	}
	return Program.findByIdAndUpdate(params, updatedProgram, {new: true}).then((result, err) => {
		if(err) {
			return err
		} else {
			return result
		}
	})
}

module.exports.archiveProgram = (params) => {

	let updatedActiveProgram = {
		isActive: false
	}
	return Program.findByIdAndUpdate(params, updatedActiveProgram, {new: true}).then((result, err) => {
		if (err){
			return false
		} else 
			return true
	})
}

module.exports.unarchiveProgram = (params) => {

	let updatedInactiveProgram = {
		isActive: true
	}
	return Program.findByIdAndUpdate(params, updatedInactiveProgram, {new: true}).then((result, err) => {
		if (err){
			return false
		} else 
			return true
	})
}

module.exports.deleteProgram = (params) => {
	return Program.findByIdAndDelete(params).then((result, err) => {
		if (err){
			return false
		} else 
			return true
	})
}