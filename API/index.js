const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors');

const port = process.env.PORT || 4000;
const app = express()

app.use(cors());
let userRoutes = require("./routes/userRoutes")
let programRoutes = require("./routes/programRoutes")

app.use(express.json());
app.use(express.urlencoded({extended: true}));


mongoose.connect("mongodb+srv://09CGarcia:admin@cluster0.sjmpx.mongodb.net/capstone-3?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)
.then(() => console.log(`Connected to Database Capstone-3`))
.catch((err) => console.log(err));

app.use("/api/users", userRoutes);
app.use("/api/programs", programRoutes);

app.listen(port, () => console.log(`Server is running at ${port}`))