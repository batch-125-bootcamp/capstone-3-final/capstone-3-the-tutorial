const mongoose = require("mongoose");
const programSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "Program name is required"]
		},
		description: {
			type: String,
			required: [true, "Program description is required"]
		},
		price: {
			type: Number,
			required: [true, "Program price is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn : {
			type: Date,
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, "userId is required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	}
);

module.exports = mongoose.model("Program", programSchema);