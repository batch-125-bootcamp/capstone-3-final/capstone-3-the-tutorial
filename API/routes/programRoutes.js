const express = require('express');
const router = express.Router();
let auth = require('./../auth')
const programController = require("./../controllers/programController");

// retrieve all active courses
router.get("/active", (req, res) => {
	programController.getAllActive().then(result => res.send(result));
})


// retriever all courses
router.get("/all", (req, res) => {
	programController.getAllProgram().then(result => res.send(result))
});

//add program 
router.post("/addProgram", auth.verify, (req, res) => {
	programController.addProgram(req.body).then(result => res.send(result))
});

// get single program
router.get("/:programId", (req, res) => {
	programController.getSingleProgram(req.params).then(result => res.send(result))
});

// edit a program
router.put('/:programId/edit', auth.verify, (req, res) => {
	// console.log(req.params.programId)
	programController.editProgram(req.params.programId, req.body).then(result => res.send(result));
});

// archive program
router.put('/:programId/archive', auth.verify, (req, res) => {
	programController.archiveProgram(req.params.programId).then(result => res.send(result));
});

// unarchive program
router.put('/:programId/unarchive', auth.verify, (req, res) => {
	programController.unarchiveProgram(req.params.programId).then(result => res.send(result));
});

// deletecourse
router.delete('/:programId/delete', auth.verify,(req,res)=>{
	//console.log(req.params.programId)
	programController.deleteProgram(req.params.programId).then(result => res.send(result));
});

module.exports = router;